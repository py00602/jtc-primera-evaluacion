package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

import datos.*;
import romano.ConversorRomano;
import utils.LogUtil;

public class ConversorRomanoThread extends Thread {
    
	//Referencia al cliente que sera atendido
    private Socket cliente;
    private int cont;
    
    public ConversorRomanoThread(Socket cliente, int cont) {
        this.cliente = cliente;
        this.cont = cont;
    }
    
    @Override
    public void run() {
    	LogUtil.INFO("Nueva peticion recibida...");
        try {
			// cuando llegue a esta linea, es porque algun cliente se conecto a este servidor
			// entonces obtenemos el OutputStream del cliente
			OutputStream clientOut = cliente.getOutputStream();
			PrintWriter pw = new PrintWriter(clientOut, true);

			// obtenemos el InputStream del cliente, para leer lo que nos dice
			InputStream clientIn = cliente.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(clientIn));

			// Leemos la primera linea del mensaje recibido
			String mensajeRecibido = br.readLine();
			
		 	int numero;
			String responseText;
			LogConversiones bean;

			// verificamos que viene en el mensaje
			if (mensajeRecibido != null) {
				if (mensajeRecibido.trim().equalsIgnoreCase("fin")) {
					
					pw.println("PROCESO FINALIZADO");
					System.exit(0);
					
				} else if (mensajeRecibido.trim().startsWith("conv")) {
					
					String strnum = mensajeRecibido.substring(4);
					numero = Integer.parseInt(mensajeRecibido.substring(4).trim());
					try {
						responseText = numero + " en romano es: " + ConversorRomano.decimal_a_romano(numero);
						pw.println(responseText);
					} catch (Exception e) {
						responseText = e.getMessage();
					}
			 									
					bean = new LogConversiones("Convercion nro " + cont, 
												cliente.getInetAddress().getHostAddress().toString(), 
												numero, 
												responseText);
					
					LogConversionesManager lcm = new LogConversionesManager();
					lcm.insertarNuevoRegistro(bean);
					pw.println("Procesado");
				}
				
			} else{
				pw.println("Mensaje recibido no valido, reintente por favor!");
			}
			
			// cerramos conexion con el cliente luego de temrinar el trabajo
			clientIn.close();
			clientOut.close();
			cliente.close();
		} catch (IOException ie) {
			System.out.println("Error al procesar Thread. ");
        }
    	LogUtil.INFO("Finalizando atencion de la peticion recibida...");
    }
    
    
}