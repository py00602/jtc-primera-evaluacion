package datos;

import java.util.Date;
/**
 * Clase LogConversiones. Representa a 1 registro de la tabla LOG_CONVERSIONES
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class LogConversiones {
    
    //TODO ok: Crear estructura de la clase, constructor, getters y setters
	private Integer idMensaje;
	private String nombreThread;
	private String IpCliente;
	private Date fechaHora;
	private Integer msgRequest;
	private String msgResponse;
	
	public LogConversiones(String nombreThread, String IpCliente, int msgRequest, String msgResponse)
	{
		this.setNombreThread(nombreThread);
		this.setIpCliente(IpCliente);
		this.fechaHora = new Date(); 
		this.setMsgRequest(msgRequest);
		this.setMsgResponse(msgResponse);
	}
	
	
	public Integer getIdMensaje() {
		return idMensaje;
	}
	public void setIdMensaje(Integer idMensaje) {
		this.idMensaje = idMensaje;
	}
	public String getNombreThread() {
		return nombreThread;
	}
	public void setNombreThread(String nombreThread) {
		this.nombreThread = nombreThread;
	}
	public String getIpCliente() {
		return IpCliente;
	}
	public void setIpCliente(String IpCliente) {
		this.IpCliente = IpCliente;
	}
	public Date getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}
	public Integer getMsgRequest() {
		return msgRequest;
	}
	public void setMsgRequest(Integer msgRequest) {
		this.msgRequest = msgRequest;
	}
	public String getMsgResponse() {
		return msgResponse;
	}
	public void setMsgResponse(String getMsgResponse) {
		this.msgResponse = getMsgResponse;
	}
	
    
} //Fin de clase
